function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S1>/Multiply-Add */
	this.urlHashMap["slowadc_conv:20"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:633,647,648,649";
	/* <S1>/Multiply-Add1 */
	this.urlHashMap["slowadc_conv:158"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:713,727,728,729";
	/* <S1>/Multiply-Add10 */
	this.urlHashMap["slowadc_conv:212"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1433,1447,1448,1449";
	/* <S1>/Multiply-Add11 */
	this.urlHashMap["slowadc_conv:218"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1513,1527,1528,1529";
	/* <S1>/Multiply-Add12 */
	this.urlHashMap["slowadc_conv:248"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1593,1607,1608,1609";
	/* <S1>/Multiply-Add13 */
	this.urlHashMap["slowadc_conv:249"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1673,1687,1688,1689";
	/* <S1>/Multiply-Add14 */
	this.urlHashMap["slowadc_conv:250"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1753,1767,1768,1769";
	/* <S1>/Multiply-Add15 */
	this.urlHashMap["slowadc_conv:251"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1833,1847,1848,1849";
	/* <S1>/Multiply-Add16 */
	this.urlHashMap["slowadc_conv:252"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1913,1927,1928,1929";
	/* <S1>/Multiply-Add17 */
	this.urlHashMap["slowadc_conv:253"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1993,2007,2008,2009";
	/* <S1>/Multiply-Add18 */
	this.urlHashMap["slowadc_conv:254"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2073,2087,2088,2089";
	/* <S1>/Multiply-Add19 */
	this.urlHashMap["slowadc_conv:255"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2153,2167,2168,2169";
	/* <S1>/Multiply-Add2 */
	this.urlHashMap["slowadc_conv:164"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:793,807,808,809";
	/* <S1>/Multiply-Add20 */
	this.urlHashMap["slowadc_conv:256"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2233,2247,2248,2249";
	/* <S1>/Multiply-Add21 */
	this.urlHashMap["slowadc_conv:418"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2313,2327,2328,2329";
	/* <S1>/Multiply-Add22 */
	this.urlHashMap["slowadc_conv:424"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2393,2407,2408,2409";
	/* <S1>/Multiply-Add23 */
	this.urlHashMap["slowadc_conv:430"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:2473,2487,2488,2489";
	/* <S1>/Multiply-Add3 */
	this.urlHashMap["slowadc_conv:170"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:873,887,888,889";
	/* <S1>/Multiply-Add4 */
	this.urlHashMap["slowadc_conv:176"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:953,967,968,969";
	/* <S1>/Multiply-Add5 */
	this.urlHashMap["slowadc_conv:182"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1033,1047,1048,1049";
	/* <S1>/Multiply-Add6 */
	this.urlHashMap["slowadc_conv:188"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1113,1127,1128,1129";
	/* <S1>/Multiply-Add7 */
	this.urlHashMap["slowadc_conv:194"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1193,1207,1208,1209";
	/* <S1>/Multiply-Add8 */
	this.urlHashMap["slowadc_conv:200"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1273,1287,1288,1289";
	/* <S1>/Multiply-Add9 */
	this.urlHashMap["slowadc_conv:206"] = "Conv_ADC_slow_Full_src_SLOW_ADC_CONV.vhd:1353,1367,1368,1369";
	/* <S3>/Product */
	this.urlHashMap["slowadc_conv:20:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:20:1324";
	/* <S3>/Sum */
	this.urlHashMap["slowadc_conv:20:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:20:1325";
	/* <S4>/Product */
	this.urlHashMap["slowadc_conv:158:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:158:1324";
	/* <S4>/Sum */
	this.urlHashMap["slowadc_conv:158:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:158:1325";
	/* <S5>/Product */
	this.urlHashMap["slowadc_conv:212:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:212:1324";
	/* <S5>/Sum */
	this.urlHashMap["slowadc_conv:212:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:212:1325";
	/* <S6>/Product */
	this.urlHashMap["slowadc_conv:218:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:218:1324";
	/* <S6>/Sum */
	this.urlHashMap["slowadc_conv:218:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:218:1325";
	/* <S7>/Product */
	this.urlHashMap["slowadc_conv:248:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:248:1324";
	/* <S7>/Sum */
	this.urlHashMap["slowadc_conv:248:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:248:1325";
	/* <S8>/Product */
	this.urlHashMap["slowadc_conv:249:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:249:1324";
	/* <S8>/Sum */
	this.urlHashMap["slowadc_conv:249:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:249:1325";
	/* <S9>/Product */
	this.urlHashMap["slowadc_conv:250:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:250:1324";
	/* <S9>/Sum */
	this.urlHashMap["slowadc_conv:250:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:250:1325";
	/* <S10>/Product */
	this.urlHashMap["slowadc_conv:251:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:251:1324";
	/* <S10>/Sum */
	this.urlHashMap["slowadc_conv:251:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:251:1325";
	/* <S11>/Product */
	this.urlHashMap["slowadc_conv:252:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:252:1324";
	/* <S11>/Sum */
	this.urlHashMap["slowadc_conv:252:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:252:1325";
	/* <S12>/Product */
	this.urlHashMap["slowadc_conv:253:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:253:1324";
	/* <S12>/Sum */
	this.urlHashMap["slowadc_conv:253:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:253:1325";
	/* <S13>/Product */
	this.urlHashMap["slowadc_conv:254:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:254:1324";
	/* <S13>/Sum */
	this.urlHashMap["slowadc_conv:254:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:254:1325";
	/* <S14>/Product */
	this.urlHashMap["slowadc_conv:255:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:255:1324";
	/* <S14>/Sum */
	this.urlHashMap["slowadc_conv:255:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:255:1325";
	/* <S15>/Product */
	this.urlHashMap["slowadc_conv:164:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:164:1324";
	/* <S15>/Sum */
	this.urlHashMap["slowadc_conv:164:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:164:1325";
	/* <S16>/Product */
	this.urlHashMap["slowadc_conv:256:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:256:1324";
	/* <S16>/Sum */
	this.urlHashMap["slowadc_conv:256:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:256:1325";
	/* <S17>/Product */
	this.urlHashMap["slowadc_conv:418:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:418:1324";
	/* <S17>/Sum */
	this.urlHashMap["slowadc_conv:418:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:418:1325";
	/* <S18>/Product */
	this.urlHashMap["slowadc_conv:424:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:424:1324";
	/* <S18>/Sum */
	this.urlHashMap["slowadc_conv:424:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:424:1325";
	/* <S19>/Product */
	this.urlHashMap["slowadc_conv:430:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:430:1324";
	/* <S19>/Sum */
	this.urlHashMap["slowadc_conv:430:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:430:1325";
	/* <S20>/Product */
	this.urlHashMap["slowadc_conv:170:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:170:1324";
	/* <S20>/Sum */
	this.urlHashMap["slowadc_conv:170:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:170:1325";
	/* <S21>/Product */
	this.urlHashMap["slowadc_conv:176:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:176:1324";
	/* <S21>/Sum */
	this.urlHashMap["slowadc_conv:176:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:176:1325";
	/* <S22>/Product */
	this.urlHashMap["slowadc_conv:182:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:182:1324";
	/* <S22>/Sum */
	this.urlHashMap["slowadc_conv:182:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:182:1325";
	/* <S23>/Product */
	this.urlHashMap["slowadc_conv:188:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:188:1324";
	/* <S23>/Sum */
	this.urlHashMap["slowadc_conv:188:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:188:1325";
	/* <S24>/Product */
	this.urlHashMap["slowadc_conv:194:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:194:1324";
	/* <S24>/Sum */
	this.urlHashMap["slowadc_conv:194:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:194:1325";
	/* <S25>/Product */
	this.urlHashMap["slowadc_conv:200:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:200:1324";
	/* <S25>/Sum */
	this.urlHashMap["slowadc_conv:200:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:200:1325";
	/* <S26>/Product */
	this.urlHashMap["slowadc_conv:206:1324"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:206:1324";
	/* <S26>/Sum */
	this.urlHashMap["slowadc_conv:206:1325"] = "msg=rtwMsg_notTraceable&block=slowadc_conv:206:1325";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "slowadc_conv"};
	this.sidHashMap["slowadc_conv"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>/ADC1"] = {sid: "slowadc_conv:62"};
	this.sidHashMap["slowadc_conv:62"] = {rtwname: "<S1>/ADC1"};
	this.rtwnameHashMap["<S1>/Gain1"] = {sid: "slowadc_conv:63"};
	this.sidHashMap["slowadc_conv:63"] = {rtwname: "<S1>/Gain1"};
	this.rtwnameHashMap["<S1>/Offset1"] = {sid: "slowadc_conv:64"};
	this.sidHashMap["slowadc_conv:64"] = {rtwname: "<S1>/Offset1"};
	this.rtwnameHashMap["<S1>/ADC2"] = {sid: "slowadc_conv:155"};
	this.sidHashMap["slowadc_conv:155"] = {rtwname: "<S1>/ADC2"};
	this.rtwnameHashMap["<S1>/Gain2"] = {sid: "slowadc_conv:156"};
	this.sidHashMap["slowadc_conv:156"] = {rtwname: "<S1>/Gain2"};
	this.rtwnameHashMap["<S1>/Offset2"] = {sid: "slowadc_conv:157"};
	this.sidHashMap["slowadc_conv:157"] = {rtwname: "<S1>/Offset2"};
	this.rtwnameHashMap["<S1>/ADC3"] = {sid: "slowadc_conv:161"};
	this.sidHashMap["slowadc_conv:161"] = {rtwname: "<S1>/ADC3"};
	this.rtwnameHashMap["<S1>/Gain3"] = {sid: "slowadc_conv:162"};
	this.sidHashMap["slowadc_conv:162"] = {rtwname: "<S1>/Gain3"};
	this.rtwnameHashMap["<S1>/Offset3"] = {sid: "slowadc_conv:163"};
	this.sidHashMap["slowadc_conv:163"] = {rtwname: "<S1>/Offset3"};
	this.rtwnameHashMap["<S1>/ADC4"] = {sid: "slowadc_conv:167"};
	this.sidHashMap["slowadc_conv:167"] = {rtwname: "<S1>/ADC4"};
	this.rtwnameHashMap["<S1>/Gain4"] = {sid: "slowadc_conv:168"};
	this.sidHashMap["slowadc_conv:168"] = {rtwname: "<S1>/Gain4"};
	this.rtwnameHashMap["<S1>/Offset4"] = {sid: "slowadc_conv:169"};
	this.sidHashMap["slowadc_conv:169"] = {rtwname: "<S1>/Offset4"};
	this.rtwnameHashMap["<S1>/ADC5"] = {sid: "slowadc_conv:173"};
	this.sidHashMap["slowadc_conv:173"] = {rtwname: "<S1>/ADC5"};
	this.rtwnameHashMap["<S1>/Gain5"] = {sid: "slowadc_conv:174"};
	this.sidHashMap["slowadc_conv:174"] = {rtwname: "<S1>/Gain5"};
	this.rtwnameHashMap["<S1>/Offset5"] = {sid: "slowadc_conv:175"};
	this.sidHashMap["slowadc_conv:175"] = {rtwname: "<S1>/Offset5"};
	this.rtwnameHashMap["<S1>/ADC6"] = {sid: "slowadc_conv:179"};
	this.sidHashMap["slowadc_conv:179"] = {rtwname: "<S1>/ADC6"};
	this.rtwnameHashMap["<S1>/Gain6"] = {sid: "slowadc_conv:180"};
	this.sidHashMap["slowadc_conv:180"] = {rtwname: "<S1>/Gain6"};
	this.rtwnameHashMap["<S1>/Offset6"] = {sid: "slowadc_conv:181"};
	this.sidHashMap["slowadc_conv:181"] = {rtwname: "<S1>/Offset6"};
	this.rtwnameHashMap["<S1>/ADC7"] = {sid: "slowadc_conv:185"};
	this.sidHashMap["slowadc_conv:185"] = {rtwname: "<S1>/ADC7"};
	this.rtwnameHashMap["<S1>/Gain7"] = {sid: "slowadc_conv:186"};
	this.sidHashMap["slowadc_conv:186"] = {rtwname: "<S1>/Gain7"};
	this.rtwnameHashMap["<S1>/Offset7"] = {sid: "slowadc_conv:187"};
	this.sidHashMap["slowadc_conv:187"] = {rtwname: "<S1>/Offset7"};
	this.rtwnameHashMap["<S1>/ADC8"] = {sid: "slowadc_conv:191"};
	this.sidHashMap["slowadc_conv:191"] = {rtwname: "<S1>/ADC8"};
	this.rtwnameHashMap["<S1>/Gain8"] = {sid: "slowadc_conv:192"};
	this.sidHashMap["slowadc_conv:192"] = {rtwname: "<S1>/Gain8"};
	this.rtwnameHashMap["<S1>/Offset8"] = {sid: "slowadc_conv:193"};
	this.sidHashMap["slowadc_conv:193"] = {rtwname: "<S1>/Offset8"};
	this.rtwnameHashMap["<S1>/ADC9"] = {sid: "slowadc_conv:197"};
	this.sidHashMap["slowadc_conv:197"] = {rtwname: "<S1>/ADC9"};
	this.rtwnameHashMap["<S1>/Gain9"] = {sid: "slowadc_conv:198"};
	this.sidHashMap["slowadc_conv:198"] = {rtwname: "<S1>/Gain9"};
	this.rtwnameHashMap["<S1>/Offset9"] = {sid: "slowadc_conv:199"};
	this.sidHashMap["slowadc_conv:199"] = {rtwname: "<S1>/Offset9"};
	this.rtwnameHashMap["<S1>/ADC10"] = {sid: "slowadc_conv:203"};
	this.sidHashMap["slowadc_conv:203"] = {rtwname: "<S1>/ADC10"};
	this.rtwnameHashMap["<S1>/Gain10"] = {sid: "slowadc_conv:204"};
	this.sidHashMap["slowadc_conv:204"] = {rtwname: "<S1>/Gain10"};
	this.rtwnameHashMap["<S1>/Offset10"] = {sid: "slowadc_conv:205"};
	this.sidHashMap["slowadc_conv:205"] = {rtwname: "<S1>/Offset10"};
	this.rtwnameHashMap["<S1>/ADC11"] = {sid: "slowadc_conv:209"};
	this.sidHashMap["slowadc_conv:209"] = {rtwname: "<S1>/ADC11"};
	this.rtwnameHashMap["<S1>/Gain11"] = {sid: "slowadc_conv:210"};
	this.sidHashMap["slowadc_conv:210"] = {rtwname: "<S1>/Gain11"};
	this.rtwnameHashMap["<S1>/Offset11"] = {sid: "slowadc_conv:211"};
	this.sidHashMap["slowadc_conv:211"] = {rtwname: "<S1>/Offset11"};
	this.rtwnameHashMap["<S1>/ADC12"] = {sid: "slowadc_conv:215"};
	this.sidHashMap["slowadc_conv:215"] = {rtwname: "<S1>/ADC12"};
	this.rtwnameHashMap["<S1>/Gain12"] = {sid: "slowadc_conv:216"};
	this.sidHashMap["slowadc_conv:216"] = {rtwname: "<S1>/Gain12"};
	this.rtwnameHashMap["<S1>/Offset12"] = {sid: "slowadc_conv:217"};
	this.sidHashMap["slowadc_conv:217"] = {rtwname: "<S1>/Offset12"};
	this.rtwnameHashMap["<S1>/ADC13"] = {sid: "slowadc_conv:221"};
	this.sidHashMap["slowadc_conv:221"] = {rtwname: "<S1>/ADC13"};
	this.rtwnameHashMap["<S1>/Gain13"] = {sid: "slowadc_conv:222"};
	this.sidHashMap["slowadc_conv:222"] = {rtwname: "<S1>/Gain13"};
	this.rtwnameHashMap["<S1>/Offset13"] = {sid: "slowadc_conv:223"};
	this.sidHashMap["slowadc_conv:223"] = {rtwname: "<S1>/Offset13"};
	this.rtwnameHashMap["<S1>/ADC14"] = {sid: "slowadc_conv:224"};
	this.sidHashMap["slowadc_conv:224"] = {rtwname: "<S1>/ADC14"};
	this.rtwnameHashMap["<S1>/Gain14"] = {sid: "slowadc_conv:225"};
	this.sidHashMap["slowadc_conv:225"] = {rtwname: "<S1>/Gain14"};
	this.rtwnameHashMap["<S1>/Offset14"] = {sid: "slowadc_conv:226"};
	this.sidHashMap["slowadc_conv:226"] = {rtwname: "<S1>/Offset14"};
	this.rtwnameHashMap["<S1>/ADC15"] = {sid: "slowadc_conv:227"};
	this.sidHashMap["slowadc_conv:227"] = {rtwname: "<S1>/ADC15"};
	this.rtwnameHashMap["<S1>/Gain15"] = {sid: "slowadc_conv:228"};
	this.sidHashMap["slowadc_conv:228"] = {rtwname: "<S1>/Gain15"};
	this.rtwnameHashMap["<S1>/Offset15"] = {sid: "slowadc_conv:229"};
	this.sidHashMap["slowadc_conv:229"] = {rtwname: "<S1>/Offset15"};
	this.rtwnameHashMap["<S1>/ADC16"] = {sid: "slowadc_conv:230"};
	this.sidHashMap["slowadc_conv:230"] = {rtwname: "<S1>/ADC16"};
	this.rtwnameHashMap["<S1>/Gain16"] = {sid: "slowadc_conv:231"};
	this.sidHashMap["slowadc_conv:231"] = {rtwname: "<S1>/Gain16"};
	this.rtwnameHashMap["<S1>/Offset16"] = {sid: "slowadc_conv:232"};
	this.sidHashMap["slowadc_conv:232"] = {rtwname: "<S1>/Offset16"};
	this.rtwnameHashMap["<S1>/ADC17"] = {sid: "slowadc_conv:233"};
	this.sidHashMap["slowadc_conv:233"] = {rtwname: "<S1>/ADC17"};
	this.rtwnameHashMap["<S1>/Gain17"] = {sid: "slowadc_conv:234"};
	this.sidHashMap["slowadc_conv:234"] = {rtwname: "<S1>/Gain17"};
	this.rtwnameHashMap["<S1>/Offset17"] = {sid: "slowadc_conv:235"};
	this.sidHashMap["slowadc_conv:235"] = {rtwname: "<S1>/Offset17"};
	this.rtwnameHashMap["<S1>/ADC18"] = {sid: "slowadc_conv:236"};
	this.sidHashMap["slowadc_conv:236"] = {rtwname: "<S1>/ADC18"};
	this.rtwnameHashMap["<S1>/Gain18"] = {sid: "slowadc_conv:237"};
	this.sidHashMap["slowadc_conv:237"] = {rtwname: "<S1>/Gain18"};
	this.rtwnameHashMap["<S1>/Offset18"] = {sid: "slowadc_conv:238"};
	this.sidHashMap["slowadc_conv:238"] = {rtwname: "<S1>/Offset18"};
	this.rtwnameHashMap["<S1>/ADC19"] = {sid: "slowadc_conv:239"};
	this.sidHashMap["slowadc_conv:239"] = {rtwname: "<S1>/ADC19"};
	this.rtwnameHashMap["<S1>/Gain19"] = {sid: "slowadc_conv:240"};
	this.sidHashMap["slowadc_conv:240"] = {rtwname: "<S1>/Gain19"};
	this.rtwnameHashMap["<S1>/Offset19"] = {sid: "slowadc_conv:241"};
	this.sidHashMap["slowadc_conv:241"] = {rtwname: "<S1>/Offset19"};
	this.rtwnameHashMap["<S1>/ADC20"] = {sid: "slowadc_conv:242"};
	this.sidHashMap["slowadc_conv:242"] = {rtwname: "<S1>/ADC20"};
	this.rtwnameHashMap["<S1>/Gain20"] = {sid: "slowadc_conv:243"};
	this.sidHashMap["slowadc_conv:243"] = {rtwname: "<S1>/Gain20"};
	this.rtwnameHashMap["<S1>/Offset20"] = {sid: "slowadc_conv:244"};
	this.sidHashMap["slowadc_conv:244"] = {rtwname: "<S1>/Offset20"};
	this.rtwnameHashMap["<S1>/ADC21"] = {sid: "slowadc_conv:245"};
	this.sidHashMap["slowadc_conv:245"] = {rtwname: "<S1>/ADC21"};
	this.rtwnameHashMap["<S1>/Gain21"] = {sid: "slowadc_conv:246"};
	this.sidHashMap["slowadc_conv:246"] = {rtwname: "<S1>/Gain21"};
	this.rtwnameHashMap["<S1>/Offset21"] = {sid: "slowadc_conv:247"};
	this.sidHashMap["slowadc_conv:247"] = {rtwname: "<S1>/Offset21"};
	this.rtwnameHashMap["<S1>/ADC22"] = {sid: "slowadc_conv:415"};
	this.sidHashMap["slowadc_conv:415"] = {rtwname: "<S1>/ADC22"};
	this.rtwnameHashMap["<S1>/Gain22"] = {sid: "slowadc_conv:416"};
	this.sidHashMap["slowadc_conv:416"] = {rtwname: "<S1>/Gain22"};
	this.rtwnameHashMap["<S1>/Offset22"] = {sid: "slowadc_conv:417"};
	this.sidHashMap["slowadc_conv:417"] = {rtwname: "<S1>/Offset22"};
	this.rtwnameHashMap["<S1>/ADC23"] = {sid: "slowadc_conv:421"};
	this.sidHashMap["slowadc_conv:421"] = {rtwname: "<S1>/ADC23"};
	this.rtwnameHashMap["<S1>/Gain23"] = {sid: "slowadc_conv:422"};
	this.sidHashMap["slowadc_conv:422"] = {rtwname: "<S1>/Gain23"};
	this.rtwnameHashMap["<S1>/Offset23"] = {sid: "slowadc_conv:423"};
	this.sidHashMap["slowadc_conv:423"] = {rtwname: "<S1>/Offset23"};
	this.rtwnameHashMap["<S1>/ADC24"] = {sid: "slowadc_conv:427"};
	this.sidHashMap["slowadc_conv:427"] = {rtwname: "<S1>/ADC24"};
	this.rtwnameHashMap["<S1>/Gain24"] = {sid: "slowadc_conv:428"};
	this.sidHashMap["slowadc_conv:428"] = {rtwname: "<S1>/Gain24"};
	this.rtwnameHashMap["<S1>/Offset24"] = {sid: "slowadc_conv:429"};
	this.sidHashMap["slowadc_conv:429"] = {rtwname: "<S1>/Offset24"};
	this.rtwnameHashMap["<S1>/Multiply-Add"] = {sid: "slowadc_conv:20"};
	this.sidHashMap["slowadc_conv:20"] = {rtwname: "<S1>/Multiply-Add"};
	this.rtwnameHashMap["<S1>/Multiply-Add1"] = {sid: "slowadc_conv:158"};
	this.sidHashMap["slowadc_conv:158"] = {rtwname: "<S1>/Multiply-Add1"};
	this.rtwnameHashMap["<S1>/Multiply-Add10"] = {sid: "slowadc_conv:212"};
	this.sidHashMap["slowadc_conv:212"] = {rtwname: "<S1>/Multiply-Add10"};
	this.rtwnameHashMap["<S1>/Multiply-Add11"] = {sid: "slowadc_conv:218"};
	this.sidHashMap["slowadc_conv:218"] = {rtwname: "<S1>/Multiply-Add11"};
	this.rtwnameHashMap["<S1>/Multiply-Add12"] = {sid: "slowadc_conv:248"};
	this.sidHashMap["slowadc_conv:248"] = {rtwname: "<S1>/Multiply-Add12"};
	this.rtwnameHashMap["<S1>/Multiply-Add13"] = {sid: "slowadc_conv:249"};
	this.sidHashMap["slowadc_conv:249"] = {rtwname: "<S1>/Multiply-Add13"};
	this.rtwnameHashMap["<S1>/Multiply-Add14"] = {sid: "slowadc_conv:250"};
	this.sidHashMap["slowadc_conv:250"] = {rtwname: "<S1>/Multiply-Add14"};
	this.rtwnameHashMap["<S1>/Multiply-Add15"] = {sid: "slowadc_conv:251"};
	this.sidHashMap["slowadc_conv:251"] = {rtwname: "<S1>/Multiply-Add15"};
	this.rtwnameHashMap["<S1>/Multiply-Add16"] = {sid: "slowadc_conv:252"};
	this.sidHashMap["slowadc_conv:252"] = {rtwname: "<S1>/Multiply-Add16"};
	this.rtwnameHashMap["<S1>/Multiply-Add17"] = {sid: "slowadc_conv:253"};
	this.sidHashMap["slowadc_conv:253"] = {rtwname: "<S1>/Multiply-Add17"};
	this.rtwnameHashMap["<S1>/Multiply-Add18"] = {sid: "slowadc_conv:254"};
	this.sidHashMap["slowadc_conv:254"] = {rtwname: "<S1>/Multiply-Add18"};
	this.rtwnameHashMap["<S1>/Multiply-Add19"] = {sid: "slowadc_conv:255"};
	this.sidHashMap["slowadc_conv:255"] = {rtwname: "<S1>/Multiply-Add19"};
	this.rtwnameHashMap["<S1>/Multiply-Add2"] = {sid: "slowadc_conv:164"};
	this.sidHashMap["slowadc_conv:164"] = {rtwname: "<S1>/Multiply-Add2"};
	this.rtwnameHashMap["<S1>/Multiply-Add20"] = {sid: "slowadc_conv:256"};
	this.sidHashMap["slowadc_conv:256"] = {rtwname: "<S1>/Multiply-Add20"};
	this.rtwnameHashMap["<S1>/Multiply-Add21"] = {sid: "slowadc_conv:418"};
	this.sidHashMap["slowadc_conv:418"] = {rtwname: "<S1>/Multiply-Add21"};
	this.rtwnameHashMap["<S1>/Multiply-Add22"] = {sid: "slowadc_conv:424"};
	this.sidHashMap["slowadc_conv:424"] = {rtwname: "<S1>/Multiply-Add22"};
	this.rtwnameHashMap["<S1>/Multiply-Add23"] = {sid: "slowadc_conv:430"};
	this.sidHashMap["slowadc_conv:430"] = {rtwname: "<S1>/Multiply-Add23"};
	this.rtwnameHashMap["<S1>/Multiply-Add3"] = {sid: "slowadc_conv:170"};
	this.sidHashMap["slowadc_conv:170"] = {rtwname: "<S1>/Multiply-Add3"};
	this.rtwnameHashMap["<S1>/Multiply-Add4"] = {sid: "slowadc_conv:176"};
	this.sidHashMap["slowadc_conv:176"] = {rtwname: "<S1>/Multiply-Add4"};
	this.rtwnameHashMap["<S1>/Multiply-Add5"] = {sid: "slowadc_conv:182"};
	this.sidHashMap["slowadc_conv:182"] = {rtwname: "<S1>/Multiply-Add5"};
	this.rtwnameHashMap["<S1>/Multiply-Add6"] = {sid: "slowadc_conv:188"};
	this.sidHashMap["slowadc_conv:188"] = {rtwname: "<S1>/Multiply-Add6"};
	this.rtwnameHashMap["<S1>/Multiply-Add7"] = {sid: "slowadc_conv:194"};
	this.sidHashMap["slowadc_conv:194"] = {rtwname: "<S1>/Multiply-Add7"};
	this.rtwnameHashMap["<S1>/Multiply-Add8"] = {sid: "slowadc_conv:200"};
	this.sidHashMap["slowadc_conv:200"] = {rtwname: "<S1>/Multiply-Add8"};
	this.rtwnameHashMap["<S1>/Multiply-Add9"] = {sid: "slowadc_conv:206"};
	this.sidHashMap["slowadc_conv:206"] = {rtwname: "<S1>/Multiply-Add9"};
	this.rtwnameHashMap["<S1>/1"] = {sid: "slowadc_conv:61"};
	this.sidHashMap["slowadc_conv:61"] = {rtwname: "<S1>/1"};
	this.rtwnameHashMap["<S1>/out_1"] = {sid: "slowadc_conv:154"};
	this.sidHashMap["slowadc_conv:154"] = {rtwname: "<S1>/out_1"};
	this.rtwnameHashMap["<S1>/2"] = {sid: "slowadc_conv:159"};
	this.sidHashMap["slowadc_conv:159"] = {rtwname: "<S1>/2"};
	this.rtwnameHashMap["<S1>/out_2"] = {sid: "slowadc_conv:160"};
	this.sidHashMap["slowadc_conv:160"] = {rtwname: "<S1>/out_2"};
	this.rtwnameHashMap["<S1>/3"] = {sid: "slowadc_conv:165"};
	this.sidHashMap["slowadc_conv:165"] = {rtwname: "<S1>/3"};
	this.rtwnameHashMap["<S1>/out_3"] = {sid: "slowadc_conv:166"};
	this.sidHashMap["slowadc_conv:166"] = {rtwname: "<S1>/out_3"};
	this.rtwnameHashMap["<S1>/4"] = {sid: "slowadc_conv:171"};
	this.sidHashMap["slowadc_conv:171"] = {rtwname: "<S1>/4"};
	this.rtwnameHashMap["<S1>/out_4"] = {sid: "slowadc_conv:172"};
	this.sidHashMap["slowadc_conv:172"] = {rtwname: "<S1>/out_4"};
	this.rtwnameHashMap["<S1>/5"] = {sid: "slowadc_conv:177"};
	this.sidHashMap["slowadc_conv:177"] = {rtwname: "<S1>/5"};
	this.rtwnameHashMap["<S1>/out_5"] = {sid: "slowadc_conv:178"};
	this.sidHashMap["slowadc_conv:178"] = {rtwname: "<S1>/out_5"};
	this.rtwnameHashMap["<S1>/6"] = {sid: "slowadc_conv:183"};
	this.sidHashMap["slowadc_conv:183"] = {rtwname: "<S1>/6"};
	this.rtwnameHashMap["<S1>/out_6"] = {sid: "slowadc_conv:184"};
	this.sidHashMap["slowadc_conv:184"] = {rtwname: "<S1>/out_6"};
	this.rtwnameHashMap["<S1>/7"] = {sid: "slowadc_conv:189"};
	this.sidHashMap["slowadc_conv:189"] = {rtwname: "<S1>/7"};
	this.rtwnameHashMap["<S1>/out_7"] = {sid: "slowadc_conv:190"};
	this.sidHashMap["slowadc_conv:190"] = {rtwname: "<S1>/out_7"};
	this.rtwnameHashMap["<S1>/8"] = {sid: "slowadc_conv:195"};
	this.sidHashMap["slowadc_conv:195"] = {rtwname: "<S1>/8"};
	this.rtwnameHashMap["<S1>/out_8"] = {sid: "slowadc_conv:196"};
	this.sidHashMap["slowadc_conv:196"] = {rtwname: "<S1>/out_8"};
	this.rtwnameHashMap["<S1>/9"] = {sid: "slowadc_conv:201"};
	this.sidHashMap["slowadc_conv:201"] = {rtwname: "<S1>/9"};
	this.rtwnameHashMap["<S1>/out_9"] = {sid: "slowadc_conv:202"};
	this.sidHashMap["slowadc_conv:202"] = {rtwname: "<S1>/out_9"};
	this.rtwnameHashMap["<S1>/10"] = {sid: "slowadc_conv:207"};
	this.sidHashMap["slowadc_conv:207"] = {rtwname: "<S1>/10"};
	this.rtwnameHashMap["<S1>/out_10"] = {sid: "slowadc_conv:208"};
	this.sidHashMap["slowadc_conv:208"] = {rtwname: "<S1>/out_10"};
	this.rtwnameHashMap["<S1>/11"] = {sid: "slowadc_conv:213"};
	this.sidHashMap["slowadc_conv:213"] = {rtwname: "<S1>/11"};
	this.rtwnameHashMap["<S1>/out_11"] = {sid: "slowadc_conv:214"};
	this.sidHashMap["slowadc_conv:214"] = {rtwname: "<S1>/out_11"};
	this.rtwnameHashMap["<S1>/12"] = {sid: "slowadc_conv:219"};
	this.sidHashMap["slowadc_conv:219"] = {rtwname: "<S1>/12"};
	this.rtwnameHashMap["<S1>/out_12"] = {sid: "slowadc_conv:220"};
	this.sidHashMap["slowadc_conv:220"] = {rtwname: "<S1>/out_12"};
	this.rtwnameHashMap["<S1>/13"] = {sid: "slowadc_conv:257"};
	this.sidHashMap["slowadc_conv:257"] = {rtwname: "<S1>/13"};
	this.rtwnameHashMap["<S1>/out_13"] = {sid: "slowadc_conv:258"};
	this.sidHashMap["slowadc_conv:258"] = {rtwname: "<S1>/out_13"};
	this.rtwnameHashMap["<S1>/14"] = {sid: "slowadc_conv:259"};
	this.sidHashMap["slowadc_conv:259"] = {rtwname: "<S1>/14"};
	this.rtwnameHashMap["<S1>/out_14"] = {sid: "slowadc_conv:260"};
	this.sidHashMap["slowadc_conv:260"] = {rtwname: "<S1>/out_14"};
	this.rtwnameHashMap["<S1>/15"] = {sid: "slowadc_conv:261"};
	this.sidHashMap["slowadc_conv:261"] = {rtwname: "<S1>/15"};
	this.rtwnameHashMap["<S1>/out_15"] = {sid: "slowadc_conv:262"};
	this.sidHashMap["slowadc_conv:262"] = {rtwname: "<S1>/out_15"};
	this.rtwnameHashMap["<S1>/16"] = {sid: "slowadc_conv:263"};
	this.sidHashMap["slowadc_conv:263"] = {rtwname: "<S1>/16"};
	this.rtwnameHashMap["<S1>/out_16"] = {sid: "slowadc_conv:264"};
	this.sidHashMap["slowadc_conv:264"] = {rtwname: "<S1>/out_16"};
	this.rtwnameHashMap["<S1>/17"] = {sid: "slowadc_conv:265"};
	this.sidHashMap["slowadc_conv:265"] = {rtwname: "<S1>/17"};
	this.rtwnameHashMap["<S1>/out_17"] = {sid: "slowadc_conv:266"};
	this.sidHashMap["slowadc_conv:266"] = {rtwname: "<S1>/out_17"};
	this.rtwnameHashMap["<S1>/18"] = {sid: "slowadc_conv:267"};
	this.sidHashMap["slowadc_conv:267"] = {rtwname: "<S1>/18"};
	this.rtwnameHashMap["<S1>/out_18"] = {sid: "slowadc_conv:268"};
	this.sidHashMap["slowadc_conv:268"] = {rtwname: "<S1>/out_18"};
	this.rtwnameHashMap["<S1>/19"] = {sid: "slowadc_conv:269"};
	this.sidHashMap["slowadc_conv:269"] = {rtwname: "<S1>/19"};
	this.rtwnameHashMap["<S1>/out_19"] = {sid: "slowadc_conv:270"};
	this.sidHashMap["slowadc_conv:270"] = {rtwname: "<S1>/out_19"};
	this.rtwnameHashMap["<S1>/20"] = {sid: "slowadc_conv:271"};
	this.sidHashMap["slowadc_conv:271"] = {rtwname: "<S1>/20"};
	this.rtwnameHashMap["<S1>/out_20"] = {sid: "slowadc_conv:272"};
	this.sidHashMap["slowadc_conv:272"] = {rtwname: "<S1>/out_20"};
	this.rtwnameHashMap["<S1>/21"] = {sid: "slowadc_conv:273"};
	this.sidHashMap["slowadc_conv:273"] = {rtwname: "<S1>/21"};
	this.rtwnameHashMap["<S1>/out_21"] = {sid: "slowadc_conv:274"};
	this.sidHashMap["slowadc_conv:274"] = {rtwname: "<S1>/out_21"};
	this.rtwnameHashMap["<S1>/22"] = {sid: "slowadc_conv:419"};
	this.sidHashMap["slowadc_conv:419"] = {rtwname: "<S1>/22"};
	this.rtwnameHashMap["<S1>/out_22"] = {sid: "slowadc_conv:420"};
	this.sidHashMap["slowadc_conv:420"] = {rtwname: "<S1>/out_22"};
	this.rtwnameHashMap["<S1>/23"] = {sid: "slowadc_conv:425"};
	this.sidHashMap["slowadc_conv:425"] = {rtwname: "<S1>/23"};
	this.rtwnameHashMap["<S1>/out_23"] = {sid: "slowadc_conv:426"};
	this.sidHashMap["slowadc_conv:426"] = {rtwname: "<S1>/out_23"};
	this.rtwnameHashMap["<S1>/24"] = {sid: "slowadc_conv:431"};
	this.sidHashMap["slowadc_conv:431"] = {rtwname: "<S1>/24"};
	this.rtwnameHashMap["<S1>/out_24"] = {sid: "slowadc_conv:432"};
	this.sidHashMap["slowadc_conv:432"] = {rtwname: "<S1>/out_24"};
	this.rtwnameHashMap["<S3>/a"] = {sid: "slowadc_conv:20:1321"};
	this.sidHashMap["slowadc_conv:20:1321"] = {rtwname: "<S3>/a"};
	this.rtwnameHashMap["<S3>/b"] = {sid: "slowadc_conv:20:1322"};
	this.sidHashMap["slowadc_conv:20:1322"] = {rtwname: "<S3>/b"};
	this.rtwnameHashMap["<S3>/c"] = {sid: "slowadc_conv:20:1323"};
	this.sidHashMap["slowadc_conv:20:1323"] = {rtwname: "<S3>/c"};
	this.rtwnameHashMap["<S3>/Product"] = {sid: "slowadc_conv:20:1324"};
	this.sidHashMap["slowadc_conv:20:1324"] = {rtwname: "<S3>/Product"};
	this.rtwnameHashMap["<S3>/Sum"] = {sid: "slowadc_conv:20:1325"};
	this.sidHashMap["slowadc_conv:20:1325"] = {rtwname: "<S3>/Sum"};
	this.rtwnameHashMap["<S3>/Out"] = {sid: "slowadc_conv:20:1326"};
	this.sidHashMap["slowadc_conv:20:1326"] = {rtwname: "<S3>/Out"};
	this.rtwnameHashMap["<S4>/a"] = {sid: "slowadc_conv:158:1321"};
	this.sidHashMap["slowadc_conv:158:1321"] = {rtwname: "<S4>/a"};
	this.rtwnameHashMap["<S4>/b"] = {sid: "slowadc_conv:158:1322"};
	this.sidHashMap["slowadc_conv:158:1322"] = {rtwname: "<S4>/b"};
	this.rtwnameHashMap["<S4>/c"] = {sid: "slowadc_conv:158:1323"};
	this.sidHashMap["slowadc_conv:158:1323"] = {rtwname: "<S4>/c"};
	this.rtwnameHashMap["<S4>/Product"] = {sid: "slowadc_conv:158:1324"};
	this.sidHashMap["slowadc_conv:158:1324"] = {rtwname: "<S4>/Product"};
	this.rtwnameHashMap["<S4>/Sum"] = {sid: "slowadc_conv:158:1325"};
	this.sidHashMap["slowadc_conv:158:1325"] = {rtwname: "<S4>/Sum"};
	this.rtwnameHashMap["<S4>/Out"] = {sid: "slowadc_conv:158:1326"};
	this.sidHashMap["slowadc_conv:158:1326"] = {rtwname: "<S4>/Out"};
	this.rtwnameHashMap["<S5>/a"] = {sid: "slowadc_conv:212:1321"};
	this.sidHashMap["slowadc_conv:212:1321"] = {rtwname: "<S5>/a"};
	this.rtwnameHashMap["<S5>/b"] = {sid: "slowadc_conv:212:1322"};
	this.sidHashMap["slowadc_conv:212:1322"] = {rtwname: "<S5>/b"};
	this.rtwnameHashMap["<S5>/c"] = {sid: "slowadc_conv:212:1323"};
	this.sidHashMap["slowadc_conv:212:1323"] = {rtwname: "<S5>/c"};
	this.rtwnameHashMap["<S5>/Product"] = {sid: "slowadc_conv:212:1324"};
	this.sidHashMap["slowadc_conv:212:1324"] = {rtwname: "<S5>/Product"};
	this.rtwnameHashMap["<S5>/Sum"] = {sid: "slowadc_conv:212:1325"};
	this.sidHashMap["slowadc_conv:212:1325"] = {rtwname: "<S5>/Sum"};
	this.rtwnameHashMap["<S5>/Out"] = {sid: "slowadc_conv:212:1326"};
	this.sidHashMap["slowadc_conv:212:1326"] = {rtwname: "<S5>/Out"};
	this.rtwnameHashMap["<S6>/a"] = {sid: "slowadc_conv:218:1321"};
	this.sidHashMap["slowadc_conv:218:1321"] = {rtwname: "<S6>/a"};
	this.rtwnameHashMap["<S6>/b"] = {sid: "slowadc_conv:218:1322"};
	this.sidHashMap["slowadc_conv:218:1322"] = {rtwname: "<S6>/b"};
	this.rtwnameHashMap["<S6>/c"] = {sid: "slowadc_conv:218:1323"};
	this.sidHashMap["slowadc_conv:218:1323"] = {rtwname: "<S6>/c"};
	this.rtwnameHashMap["<S6>/Product"] = {sid: "slowadc_conv:218:1324"};
	this.sidHashMap["slowadc_conv:218:1324"] = {rtwname: "<S6>/Product"};
	this.rtwnameHashMap["<S6>/Sum"] = {sid: "slowadc_conv:218:1325"};
	this.sidHashMap["slowadc_conv:218:1325"] = {rtwname: "<S6>/Sum"};
	this.rtwnameHashMap["<S6>/Out"] = {sid: "slowadc_conv:218:1326"};
	this.sidHashMap["slowadc_conv:218:1326"] = {rtwname: "<S6>/Out"};
	this.rtwnameHashMap["<S7>/a"] = {sid: "slowadc_conv:248:1321"};
	this.sidHashMap["slowadc_conv:248:1321"] = {rtwname: "<S7>/a"};
	this.rtwnameHashMap["<S7>/b"] = {sid: "slowadc_conv:248:1322"};
	this.sidHashMap["slowadc_conv:248:1322"] = {rtwname: "<S7>/b"};
	this.rtwnameHashMap["<S7>/c"] = {sid: "slowadc_conv:248:1323"};
	this.sidHashMap["slowadc_conv:248:1323"] = {rtwname: "<S7>/c"};
	this.rtwnameHashMap["<S7>/Product"] = {sid: "slowadc_conv:248:1324"};
	this.sidHashMap["slowadc_conv:248:1324"] = {rtwname: "<S7>/Product"};
	this.rtwnameHashMap["<S7>/Sum"] = {sid: "slowadc_conv:248:1325"};
	this.sidHashMap["slowadc_conv:248:1325"] = {rtwname: "<S7>/Sum"};
	this.rtwnameHashMap["<S7>/Out"] = {sid: "slowadc_conv:248:1326"};
	this.sidHashMap["slowadc_conv:248:1326"] = {rtwname: "<S7>/Out"};
	this.rtwnameHashMap["<S8>/a"] = {sid: "slowadc_conv:249:1321"};
	this.sidHashMap["slowadc_conv:249:1321"] = {rtwname: "<S8>/a"};
	this.rtwnameHashMap["<S8>/b"] = {sid: "slowadc_conv:249:1322"};
	this.sidHashMap["slowadc_conv:249:1322"] = {rtwname: "<S8>/b"};
	this.rtwnameHashMap["<S8>/c"] = {sid: "slowadc_conv:249:1323"};
	this.sidHashMap["slowadc_conv:249:1323"] = {rtwname: "<S8>/c"};
	this.rtwnameHashMap["<S8>/Product"] = {sid: "slowadc_conv:249:1324"};
	this.sidHashMap["slowadc_conv:249:1324"] = {rtwname: "<S8>/Product"};
	this.rtwnameHashMap["<S8>/Sum"] = {sid: "slowadc_conv:249:1325"};
	this.sidHashMap["slowadc_conv:249:1325"] = {rtwname: "<S8>/Sum"};
	this.rtwnameHashMap["<S8>/Out"] = {sid: "slowadc_conv:249:1326"};
	this.sidHashMap["slowadc_conv:249:1326"] = {rtwname: "<S8>/Out"};
	this.rtwnameHashMap["<S9>/a"] = {sid: "slowadc_conv:250:1321"};
	this.sidHashMap["slowadc_conv:250:1321"] = {rtwname: "<S9>/a"};
	this.rtwnameHashMap["<S9>/b"] = {sid: "slowadc_conv:250:1322"};
	this.sidHashMap["slowadc_conv:250:1322"] = {rtwname: "<S9>/b"};
	this.rtwnameHashMap["<S9>/c"] = {sid: "slowadc_conv:250:1323"};
	this.sidHashMap["slowadc_conv:250:1323"] = {rtwname: "<S9>/c"};
	this.rtwnameHashMap["<S9>/Product"] = {sid: "slowadc_conv:250:1324"};
	this.sidHashMap["slowadc_conv:250:1324"] = {rtwname: "<S9>/Product"};
	this.rtwnameHashMap["<S9>/Sum"] = {sid: "slowadc_conv:250:1325"};
	this.sidHashMap["slowadc_conv:250:1325"] = {rtwname: "<S9>/Sum"};
	this.rtwnameHashMap["<S9>/Out"] = {sid: "slowadc_conv:250:1326"};
	this.sidHashMap["slowadc_conv:250:1326"] = {rtwname: "<S9>/Out"};
	this.rtwnameHashMap["<S10>/a"] = {sid: "slowadc_conv:251:1321"};
	this.sidHashMap["slowadc_conv:251:1321"] = {rtwname: "<S10>/a"};
	this.rtwnameHashMap["<S10>/b"] = {sid: "slowadc_conv:251:1322"};
	this.sidHashMap["slowadc_conv:251:1322"] = {rtwname: "<S10>/b"};
	this.rtwnameHashMap["<S10>/c"] = {sid: "slowadc_conv:251:1323"};
	this.sidHashMap["slowadc_conv:251:1323"] = {rtwname: "<S10>/c"};
	this.rtwnameHashMap["<S10>/Product"] = {sid: "slowadc_conv:251:1324"};
	this.sidHashMap["slowadc_conv:251:1324"] = {rtwname: "<S10>/Product"};
	this.rtwnameHashMap["<S10>/Sum"] = {sid: "slowadc_conv:251:1325"};
	this.sidHashMap["slowadc_conv:251:1325"] = {rtwname: "<S10>/Sum"};
	this.rtwnameHashMap["<S10>/Out"] = {sid: "slowadc_conv:251:1326"};
	this.sidHashMap["slowadc_conv:251:1326"] = {rtwname: "<S10>/Out"};
	this.rtwnameHashMap["<S11>/a"] = {sid: "slowadc_conv:252:1321"};
	this.sidHashMap["slowadc_conv:252:1321"] = {rtwname: "<S11>/a"};
	this.rtwnameHashMap["<S11>/b"] = {sid: "slowadc_conv:252:1322"};
	this.sidHashMap["slowadc_conv:252:1322"] = {rtwname: "<S11>/b"};
	this.rtwnameHashMap["<S11>/c"] = {sid: "slowadc_conv:252:1323"};
	this.sidHashMap["slowadc_conv:252:1323"] = {rtwname: "<S11>/c"};
	this.rtwnameHashMap["<S11>/Product"] = {sid: "slowadc_conv:252:1324"};
	this.sidHashMap["slowadc_conv:252:1324"] = {rtwname: "<S11>/Product"};
	this.rtwnameHashMap["<S11>/Sum"] = {sid: "slowadc_conv:252:1325"};
	this.sidHashMap["slowadc_conv:252:1325"] = {rtwname: "<S11>/Sum"};
	this.rtwnameHashMap["<S11>/Out"] = {sid: "slowadc_conv:252:1326"};
	this.sidHashMap["slowadc_conv:252:1326"] = {rtwname: "<S11>/Out"};
	this.rtwnameHashMap["<S12>/a"] = {sid: "slowadc_conv:253:1321"};
	this.sidHashMap["slowadc_conv:253:1321"] = {rtwname: "<S12>/a"};
	this.rtwnameHashMap["<S12>/b"] = {sid: "slowadc_conv:253:1322"};
	this.sidHashMap["slowadc_conv:253:1322"] = {rtwname: "<S12>/b"};
	this.rtwnameHashMap["<S12>/c"] = {sid: "slowadc_conv:253:1323"};
	this.sidHashMap["slowadc_conv:253:1323"] = {rtwname: "<S12>/c"};
	this.rtwnameHashMap["<S12>/Product"] = {sid: "slowadc_conv:253:1324"};
	this.sidHashMap["slowadc_conv:253:1324"] = {rtwname: "<S12>/Product"};
	this.rtwnameHashMap["<S12>/Sum"] = {sid: "slowadc_conv:253:1325"};
	this.sidHashMap["slowadc_conv:253:1325"] = {rtwname: "<S12>/Sum"};
	this.rtwnameHashMap["<S12>/Out"] = {sid: "slowadc_conv:253:1326"};
	this.sidHashMap["slowadc_conv:253:1326"] = {rtwname: "<S12>/Out"};
	this.rtwnameHashMap["<S13>/a"] = {sid: "slowadc_conv:254:1321"};
	this.sidHashMap["slowadc_conv:254:1321"] = {rtwname: "<S13>/a"};
	this.rtwnameHashMap["<S13>/b"] = {sid: "slowadc_conv:254:1322"};
	this.sidHashMap["slowadc_conv:254:1322"] = {rtwname: "<S13>/b"};
	this.rtwnameHashMap["<S13>/c"] = {sid: "slowadc_conv:254:1323"};
	this.sidHashMap["slowadc_conv:254:1323"] = {rtwname: "<S13>/c"};
	this.rtwnameHashMap["<S13>/Product"] = {sid: "slowadc_conv:254:1324"};
	this.sidHashMap["slowadc_conv:254:1324"] = {rtwname: "<S13>/Product"};
	this.rtwnameHashMap["<S13>/Sum"] = {sid: "slowadc_conv:254:1325"};
	this.sidHashMap["slowadc_conv:254:1325"] = {rtwname: "<S13>/Sum"};
	this.rtwnameHashMap["<S13>/Out"] = {sid: "slowadc_conv:254:1326"};
	this.sidHashMap["slowadc_conv:254:1326"] = {rtwname: "<S13>/Out"};
	this.rtwnameHashMap["<S14>/a"] = {sid: "slowadc_conv:255:1321"};
	this.sidHashMap["slowadc_conv:255:1321"] = {rtwname: "<S14>/a"};
	this.rtwnameHashMap["<S14>/b"] = {sid: "slowadc_conv:255:1322"};
	this.sidHashMap["slowadc_conv:255:1322"] = {rtwname: "<S14>/b"};
	this.rtwnameHashMap["<S14>/c"] = {sid: "slowadc_conv:255:1323"};
	this.sidHashMap["slowadc_conv:255:1323"] = {rtwname: "<S14>/c"};
	this.rtwnameHashMap["<S14>/Product"] = {sid: "slowadc_conv:255:1324"};
	this.sidHashMap["slowadc_conv:255:1324"] = {rtwname: "<S14>/Product"};
	this.rtwnameHashMap["<S14>/Sum"] = {sid: "slowadc_conv:255:1325"};
	this.sidHashMap["slowadc_conv:255:1325"] = {rtwname: "<S14>/Sum"};
	this.rtwnameHashMap["<S14>/Out"] = {sid: "slowadc_conv:255:1326"};
	this.sidHashMap["slowadc_conv:255:1326"] = {rtwname: "<S14>/Out"};
	this.rtwnameHashMap["<S15>/a"] = {sid: "slowadc_conv:164:1321"};
	this.sidHashMap["slowadc_conv:164:1321"] = {rtwname: "<S15>/a"};
	this.rtwnameHashMap["<S15>/b"] = {sid: "slowadc_conv:164:1322"};
	this.sidHashMap["slowadc_conv:164:1322"] = {rtwname: "<S15>/b"};
	this.rtwnameHashMap["<S15>/c"] = {sid: "slowadc_conv:164:1323"};
	this.sidHashMap["slowadc_conv:164:1323"] = {rtwname: "<S15>/c"};
	this.rtwnameHashMap["<S15>/Product"] = {sid: "slowadc_conv:164:1324"};
	this.sidHashMap["slowadc_conv:164:1324"] = {rtwname: "<S15>/Product"};
	this.rtwnameHashMap["<S15>/Sum"] = {sid: "slowadc_conv:164:1325"};
	this.sidHashMap["slowadc_conv:164:1325"] = {rtwname: "<S15>/Sum"};
	this.rtwnameHashMap["<S15>/Out"] = {sid: "slowadc_conv:164:1326"};
	this.sidHashMap["slowadc_conv:164:1326"] = {rtwname: "<S15>/Out"};
	this.rtwnameHashMap["<S16>/a"] = {sid: "slowadc_conv:256:1321"};
	this.sidHashMap["slowadc_conv:256:1321"] = {rtwname: "<S16>/a"};
	this.rtwnameHashMap["<S16>/b"] = {sid: "slowadc_conv:256:1322"};
	this.sidHashMap["slowadc_conv:256:1322"] = {rtwname: "<S16>/b"};
	this.rtwnameHashMap["<S16>/c"] = {sid: "slowadc_conv:256:1323"};
	this.sidHashMap["slowadc_conv:256:1323"] = {rtwname: "<S16>/c"};
	this.rtwnameHashMap["<S16>/Product"] = {sid: "slowadc_conv:256:1324"};
	this.sidHashMap["slowadc_conv:256:1324"] = {rtwname: "<S16>/Product"};
	this.rtwnameHashMap["<S16>/Sum"] = {sid: "slowadc_conv:256:1325"};
	this.sidHashMap["slowadc_conv:256:1325"] = {rtwname: "<S16>/Sum"};
	this.rtwnameHashMap["<S16>/Out"] = {sid: "slowadc_conv:256:1326"};
	this.sidHashMap["slowadc_conv:256:1326"] = {rtwname: "<S16>/Out"};
	this.rtwnameHashMap["<S17>/a"] = {sid: "slowadc_conv:418:1321"};
	this.sidHashMap["slowadc_conv:418:1321"] = {rtwname: "<S17>/a"};
	this.rtwnameHashMap["<S17>/b"] = {sid: "slowadc_conv:418:1322"};
	this.sidHashMap["slowadc_conv:418:1322"] = {rtwname: "<S17>/b"};
	this.rtwnameHashMap["<S17>/c"] = {sid: "slowadc_conv:418:1323"};
	this.sidHashMap["slowadc_conv:418:1323"] = {rtwname: "<S17>/c"};
	this.rtwnameHashMap["<S17>/Product"] = {sid: "slowadc_conv:418:1324"};
	this.sidHashMap["slowadc_conv:418:1324"] = {rtwname: "<S17>/Product"};
	this.rtwnameHashMap["<S17>/Sum"] = {sid: "slowadc_conv:418:1325"};
	this.sidHashMap["slowadc_conv:418:1325"] = {rtwname: "<S17>/Sum"};
	this.rtwnameHashMap["<S17>/Out"] = {sid: "slowadc_conv:418:1326"};
	this.sidHashMap["slowadc_conv:418:1326"] = {rtwname: "<S17>/Out"};
	this.rtwnameHashMap["<S18>/a"] = {sid: "slowadc_conv:424:1321"};
	this.sidHashMap["slowadc_conv:424:1321"] = {rtwname: "<S18>/a"};
	this.rtwnameHashMap["<S18>/b"] = {sid: "slowadc_conv:424:1322"};
	this.sidHashMap["slowadc_conv:424:1322"] = {rtwname: "<S18>/b"};
	this.rtwnameHashMap["<S18>/c"] = {sid: "slowadc_conv:424:1323"};
	this.sidHashMap["slowadc_conv:424:1323"] = {rtwname: "<S18>/c"};
	this.rtwnameHashMap["<S18>/Product"] = {sid: "slowadc_conv:424:1324"};
	this.sidHashMap["slowadc_conv:424:1324"] = {rtwname: "<S18>/Product"};
	this.rtwnameHashMap["<S18>/Sum"] = {sid: "slowadc_conv:424:1325"};
	this.sidHashMap["slowadc_conv:424:1325"] = {rtwname: "<S18>/Sum"};
	this.rtwnameHashMap["<S18>/Out"] = {sid: "slowadc_conv:424:1326"};
	this.sidHashMap["slowadc_conv:424:1326"] = {rtwname: "<S18>/Out"};
	this.rtwnameHashMap["<S19>/a"] = {sid: "slowadc_conv:430:1321"};
	this.sidHashMap["slowadc_conv:430:1321"] = {rtwname: "<S19>/a"};
	this.rtwnameHashMap["<S19>/b"] = {sid: "slowadc_conv:430:1322"};
	this.sidHashMap["slowadc_conv:430:1322"] = {rtwname: "<S19>/b"};
	this.rtwnameHashMap["<S19>/c"] = {sid: "slowadc_conv:430:1323"};
	this.sidHashMap["slowadc_conv:430:1323"] = {rtwname: "<S19>/c"};
	this.rtwnameHashMap["<S19>/Product"] = {sid: "slowadc_conv:430:1324"};
	this.sidHashMap["slowadc_conv:430:1324"] = {rtwname: "<S19>/Product"};
	this.rtwnameHashMap["<S19>/Sum"] = {sid: "slowadc_conv:430:1325"};
	this.sidHashMap["slowadc_conv:430:1325"] = {rtwname: "<S19>/Sum"};
	this.rtwnameHashMap["<S19>/Out"] = {sid: "slowadc_conv:430:1326"};
	this.sidHashMap["slowadc_conv:430:1326"] = {rtwname: "<S19>/Out"};
	this.rtwnameHashMap["<S20>/a"] = {sid: "slowadc_conv:170:1321"};
	this.sidHashMap["slowadc_conv:170:1321"] = {rtwname: "<S20>/a"};
	this.rtwnameHashMap["<S20>/b"] = {sid: "slowadc_conv:170:1322"};
	this.sidHashMap["slowadc_conv:170:1322"] = {rtwname: "<S20>/b"};
	this.rtwnameHashMap["<S20>/c"] = {sid: "slowadc_conv:170:1323"};
	this.sidHashMap["slowadc_conv:170:1323"] = {rtwname: "<S20>/c"};
	this.rtwnameHashMap["<S20>/Product"] = {sid: "slowadc_conv:170:1324"};
	this.sidHashMap["slowadc_conv:170:1324"] = {rtwname: "<S20>/Product"};
	this.rtwnameHashMap["<S20>/Sum"] = {sid: "slowadc_conv:170:1325"};
	this.sidHashMap["slowadc_conv:170:1325"] = {rtwname: "<S20>/Sum"};
	this.rtwnameHashMap["<S20>/Out"] = {sid: "slowadc_conv:170:1326"};
	this.sidHashMap["slowadc_conv:170:1326"] = {rtwname: "<S20>/Out"};
	this.rtwnameHashMap["<S21>/a"] = {sid: "slowadc_conv:176:1321"};
	this.sidHashMap["slowadc_conv:176:1321"] = {rtwname: "<S21>/a"};
	this.rtwnameHashMap["<S21>/b"] = {sid: "slowadc_conv:176:1322"};
	this.sidHashMap["slowadc_conv:176:1322"] = {rtwname: "<S21>/b"};
	this.rtwnameHashMap["<S21>/c"] = {sid: "slowadc_conv:176:1323"};
	this.sidHashMap["slowadc_conv:176:1323"] = {rtwname: "<S21>/c"};
	this.rtwnameHashMap["<S21>/Product"] = {sid: "slowadc_conv:176:1324"};
	this.sidHashMap["slowadc_conv:176:1324"] = {rtwname: "<S21>/Product"};
	this.rtwnameHashMap["<S21>/Sum"] = {sid: "slowadc_conv:176:1325"};
	this.sidHashMap["slowadc_conv:176:1325"] = {rtwname: "<S21>/Sum"};
	this.rtwnameHashMap["<S21>/Out"] = {sid: "slowadc_conv:176:1326"};
	this.sidHashMap["slowadc_conv:176:1326"] = {rtwname: "<S21>/Out"};
	this.rtwnameHashMap["<S22>/a"] = {sid: "slowadc_conv:182:1321"};
	this.sidHashMap["slowadc_conv:182:1321"] = {rtwname: "<S22>/a"};
	this.rtwnameHashMap["<S22>/b"] = {sid: "slowadc_conv:182:1322"};
	this.sidHashMap["slowadc_conv:182:1322"] = {rtwname: "<S22>/b"};
	this.rtwnameHashMap["<S22>/c"] = {sid: "slowadc_conv:182:1323"};
	this.sidHashMap["slowadc_conv:182:1323"] = {rtwname: "<S22>/c"};
	this.rtwnameHashMap["<S22>/Product"] = {sid: "slowadc_conv:182:1324"};
	this.sidHashMap["slowadc_conv:182:1324"] = {rtwname: "<S22>/Product"};
	this.rtwnameHashMap["<S22>/Sum"] = {sid: "slowadc_conv:182:1325"};
	this.sidHashMap["slowadc_conv:182:1325"] = {rtwname: "<S22>/Sum"};
	this.rtwnameHashMap["<S22>/Out"] = {sid: "slowadc_conv:182:1326"};
	this.sidHashMap["slowadc_conv:182:1326"] = {rtwname: "<S22>/Out"};
	this.rtwnameHashMap["<S23>/a"] = {sid: "slowadc_conv:188:1321"};
	this.sidHashMap["slowadc_conv:188:1321"] = {rtwname: "<S23>/a"};
	this.rtwnameHashMap["<S23>/b"] = {sid: "slowadc_conv:188:1322"};
	this.sidHashMap["slowadc_conv:188:1322"] = {rtwname: "<S23>/b"};
	this.rtwnameHashMap["<S23>/c"] = {sid: "slowadc_conv:188:1323"};
	this.sidHashMap["slowadc_conv:188:1323"] = {rtwname: "<S23>/c"};
	this.rtwnameHashMap["<S23>/Product"] = {sid: "slowadc_conv:188:1324"};
	this.sidHashMap["slowadc_conv:188:1324"] = {rtwname: "<S23>/Product"};
	this.rtwnameHashMap["<S23>/Sum"] = {sid: "slowadc_conv:188:1325"};
	this.sidHashMap["slowadc_conv:188:1325"] = {rtwname: "<S23>/Sum"};
	this.rtwnameHashMap["<S23>/Out"] = {sid: "slowadc_conv:188:1326"};
	this.sidHashMap["slowadc_conv:188:1326"] = {rtwname: "<S23>/Out"};
	this.rtwnameHashMap["<S24>/a"] = {sid: "slowadc_conv:194:1321"};
	this.sidHashMap["slowadc_conv:194:1321"] = {rtwname: "<S24>/a"};
	this.rtwnameHashMap["<S24>/b"] = {sid: "slowadc_conv:194:1322"};
	this.sidHashMap["slowadc_conv:194:1322"] = {rtwname: "<S24>/b"};
	this.rtwnameHashMap["<S24>/c"] = {sid: "slowadc_conv:194:1323"};
	this.sidHashMap["slowadc_conv:194:1323"] = {rtwname: "<S24>/c"};
	this.rtwnameHashMap["<S24>/Product"] = {sid: "slowadc_conv:194:1324"};
	this.sidHashMap["slowadc_conv:194:1324"] = {rtwname: "<S24>/Product"};
	this.rtwnameHashMap["<S24>/Sum"] = {sid: "slowadc_conv:194:1325"};
	this.sidHashMap["slowadc_conv:194:1325"] = {rtwname: "<S24>/Sum"};
	this.rtwnameHashMap["<S24>/Out"] = {sid: "slowadc_conv:194:1326"};
	this.sidHashMap["slowadc_conv:194:1326"] = {rtwname: "<S24>/Out"};
	this.rtwnameHashMap["<S25>/a"] = {sid: "slowadc_conv:200:1321"};
	this.sidHashMap["slowadc_conv:200:1321"] = {rtwname: "<S25>/a"};
	this.rtwnameHashMap["<S25>/b"] = {sid: "slowadc_conv:200:1322"};
	this.sidHashMap["slowadc_conv:200:1322"] = {rtwname: "<S25>/b"};
	this.rtwnameHashMap["<S25>/c"] = {sid: "slowadc_conv:200:1323"};
	this.sidHashMap["slowadc_conv:200:1323"] = {rtwname: "<S25>/c"};
	this.rtwnameHashMap["<S25>/Product"] = {sid: "slowadc_conv:200:1324"};
	this.sidHashMap["slowadc_conv:200:1324"] = {rtwname: "<S25>/Product"};
	this.rtwnameHashMap["<S25>/Sum"] = {sid: "slowadc_conv:200:1325"};
	this.sidHashMap["slowadc_conv:200:1325"] = {rtwname: "<S25>/Sum"};
	this.rtwnameHashMap["<S25>/Out"] = {sid: "slowadc_conv:200:1326"};
	this.sidHashMap["slowadc_conv:200:1326"] = {rtwname: "<S25>/Out"};
	this.rtwnameHashMap["<S26>/a"] = {sid: "slowadc_conv:206:1321"};
	this.sidHashMap["slowadc_conv:206:1321"] = {rtwname: "<S26>/a"};
	this.rtwnameHashMap["<S26>/b"] = {sid: "slowadc_conv:206:1322"};
	this.sidHashMap["slowadc_conv:206:1322"] = {rtwname: "<S26>/b"};
	this.rtwnameHashMap["<S26>/c"] = {sid: "slowadc_conv:206:1323"};
	this.sidHashMap["slowadc_conv:206:1323"] = {rtwname: "<S26>/c"};
	this.rtwnameHashMap["<S26>/Product"] = {sid: "slowadc_conv:206:1324"};
	this.sidHashMap["slowadc_conv:206:1324"] = {rtwname: "<S26>/Product"};
	this.rtwnameHashMap["<S26>/Sum"] = {sid: "slowadc_conv:206:1325"};
	this.sidHashMap["slowadc_conv:206:1325"] = {rtwname: "<S26>/Sum"};
	this.rtwnameHashMap["<S26>/Out"] = {sid: "slowadc_conv:206:1326"};
	this.sidHashMap["slowadc_conv:206:1326"] = {rtwname: "<S26>/Out"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
